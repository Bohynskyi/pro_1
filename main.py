from tkinter import *
import random

COLOR = [
    "#ff0000", "#9999ff", "#00ff00",
    "#993300", "#003300", "#00ccff"
]



class Timer:
    def __init__(self, parent, closk):
        # variable storing time
        self.closk = closk
        self.seconds = 0

        if self.closk:

            self.C = Canvas(parent, bg="white", width=300, height=300)
            self.C.place(x=0, y=0)
            coord = 5, 5, 295, 295
            self.arc = self.C.create_arc((10, 10, 290, 290), start=90, extent=3, fill="black")
            self.oval = self.C.create_oval(coord)

            self.C.pack()
            self.C.after(1000, self.refresh_label)
        else:

            # label displaying time
            self.label = Label(parent, text="0 s", font="Arial 30", width=10)
            self.label.pack()
            # start the timer
            self.label.after(1000, self.refresh_label)

    def refresh_label(self):
        if self.closk:
            self.seconds += 1
            self.C.itemconfig(self.arc, start=90 - (self.seconds * 6))
            self.C.after(1000, self.refresh_label)
        else:
            self.seconds += 1
            self.label.configure(text="%i s" % self.seconds)
            self.label.after(1000, self.refresh_label)


class Main(Frame):
    def __init__(self, root):
        super(Main, self).__init__(root)
        self.bytom_color = "#FFF"
        self.zegar = True
        self.lable()
        self.build()

    def lable(self):
        self.formula = "0"
        self.lbl = Label(text=self.formula, font=("Times New Roman", 21, "bold"), bg="#000", foreground="#FFF")
        self.lbl.place(x=11, y=50)

    def build(self):
        btns = [
            "Zmiana tła", 'Wyświetl zegar',
            "C", "DEL", "*", "=",
            "1", "2", "3", "/",
            "4", "5", "6", "+",
            "7", "8", "9", "-",
            "(", "0", ")", "X^2"
        ]

        x = 10
        y = 140
        for bt in btns:
            com = lambda x=bt: self.logicalc(x)
            if bt in ['Zmiana tła', 'Wyświetl zegar']:
                self.bytton = Button(text=bt, bg=self.bytom_color,
                                     font=("Times New Roman", 15),
                                     command=com).place(x=x, y=y,
                                                        width=232,
                                                        height=79)
                x += 117 * 2
                if x > 400:
                    x = 10
                    y += 81
            else:
                self.bytton = Button(text=bt, bg=self.bytom_color,
                                     font=("Times New Roman", 15),
                                     command=com).place(x=x, y=y,
                                                        width=115,
                                                        height=79)
                x += 117
                if x > 400:
                    x = 10
                    y += 81

    def logicalc(self, operation):
        if operation == "C":
            operation = ""
            self.formula = ""

        elif operation == "Wyświetl zegar":
            if self.zegar:
                self.window = Toplevel(root)
                self.window.title("Zegar")
                self.timer = Timer(self.window,True)
                self.zegar = False
            else:
                self.window = Toplevel(root)
                self.window.title("Zegar")
                self.timer = Timer(self.window, False)
                self.zegar = True
        elif operation == "Zmiana tła":
            operation = ""
            color = COLOR[0]
            del COLOR[0]
            COLOR.append(color)
            root.configure(bg=color)
            self.lbl.configure(bg=color)
            operation = ""

        elif operation == "DEL":
            operation = ""
            self.formula = self.formula[0:-1]

        elif operation == "X^2":
            operation = ""
            self.formula = str((eval(self.formula)) ** 2)

        elif operation == "=":
            try:
                self.formula = str(eval(self.formula))
            except:
                self.formula = "Error"
        else:
            if self.formula == "0" or self.formula == "Error":
                self.formula = ""
            self.formula += operation
        self.update()

    def update(self):
        if self.formula == "":
            self.formula = "0"
        self.lbl.configure(text=self.formula)

    def key(self, event):
        operation = event.keysym
        if operation in ["1", "2", "3", "/",
                         "4", "5", "6", "+",
                         "7", "8", "9", "-", ]:
            self.logicalc(operation)
        elif operation == "plus":
            self.logicalc("+")
        elif operation == "minus":
            self.logicalc("-")
        elif operation == "Return":
            self.logicalc("=")
        elif operation == "slash":
            self.logicalc("/")
        elif operation == "asterisk":
            self.logicalc("*")
        elif operation == "BackSpace":
            self.logicalc("DEL")
        elif operation == "parenleft":  # DODANE
            self.logicalc("(")
        elif operation == "parenright":  # DODANE
            self.logicalc(")")


if __name__ == '__main__':
    root = Tk()
    root["bg"] = "#000"
    root.geometry("485x650+200+200")
    root.title("Kalkulator")
    root.resizable(False, False)
    app = Main(root)


    app.pack()
    root.bind_all('<Key>', app.key)
    root.mainloop()
